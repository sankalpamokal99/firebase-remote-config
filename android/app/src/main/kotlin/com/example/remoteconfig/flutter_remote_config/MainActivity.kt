package com.example.remoteconfig.flutter_remote_config

import android.app.NotificationManager
import android.content.Context
import android.util.Log
import androidx.annotation.NonNull
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel

class MainActivity: FlutterActivity() {
    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, "samples.flutter.dev/removeNotification").setMethodCallHandler {
            call, result ->
            if(call.method == "clearAllAppNotifications") {
                print("clearAllAppNotifications");
                val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.cancelAll()
                result.success(true)
            }else if (call.method == "clearAppNotificationsByTag") {
//                print("clearAppNotificationsByTag");
//            val tag = call.argument<String>("tag")
//            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
//            val notifications = notificationManager.activeNotifications
//            for (notification in notifications) {
//                if (notification.tag == "news") {
//                    notificationManager.cancel("news", notification.id)
//                }
//            }
                Log.d("TAG", "clearAllAppNotifications")

                val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                val notifications = notificationManager.activeNotifications
                for (notification in notifications) {
                    val message = intent.extras
                    Log.d("TAG", "your log message$message")
//                    if (message == "news") {
                    notificationManager.cancel("news", notification.id)
//                }
                }
                result.success(true)
            }
            else {
                result.notImplemented()
            }
        }
    }
}