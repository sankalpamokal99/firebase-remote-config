import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:socket_io_client/socket_io_client.dart' as ws;

ws.Socket? socket;
const platform = MethodChannel('samples.flutter.dev/removeNotification');

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // await Firebase.initializeApp();
  // await RemoteConfigService().initialize();
  // await callLocalApi();
  initializeSocket();
  // await NotificationService().setupInteractedMessage();

  // initializeNotificationService();
  runApp(const MyApp());
  // RemoteMessage? initialMessage =
  //     await FirebaseMessaging.instance.getInitialMessage();
  // if (initialMessage != null) {
  //   // App received a notification when it was killed
  // }
}

void initializeMethodChannel() {}

void initializeNotificationService() {}

void initializeSocket() {
  socket = ws.io(
      'http://localhost:4000',
      ws.OptionBuilder().disableAutoConnect().setTransports(['websocket'])
          // .setPath('/api/v1/socket')
          // .setAuth({'token': authToken})
          .build());
  socket?.connect();
  socket?.onConnect((_) {
    print('connected');
    socket?.on('broadcast', (data) => print(data));
    socket?.onDisconnect((_) => print('disconnect'));
    socket?.on('fromServer', (_) => print(_));
    socket?.on('removeNotification',
        (_) => removeNotification(all: false, tag: _['tag']));
  });
}

Future<void> removeNotification({required bool all, String? tag}) async {
  print("removeNotification called");
  // try {
  //   print("tag $tag");
  //
  //   final bool result = await platform.invokeMethod(
  //       all ? 'clearAllAppNotifications' : 'clearAppNotificationsByTag', [
  //     {'tag': tag}
  //   ]);
  //   print("result $result");
  // } on PlatformException catch (e) {
  //   print("error ${e.toString()}");
  // }
}

callLocalApi() async {
  String url = "http://localhost/";
  http.Response response = await http.get(Uri.parse(url));
  print("response ${response.body}");
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      // home: const MyHomePage(title: 'Flutter Remote Config'),
      home: Scaffold(
          body: Column(
        children: [
          InkWell(
            child: Container(
              height: 200,
              width: 200,
              color: Colors.red,
            ),
            onTap: () {
              print("clicked");
              // socket?.emit('test', 'Msg from client');
              // removeNotification(all: false);
            },
          ),
          InkWell(
            child: Container(
              height: 200,
              width: 200,
              color: Colors.green,
            ),
            onTap: () {
              print("clicked");
              socket?.emit('temp', {'data': 'tempdata'});
              // removeNotification(all: true);
            },
          ),
        ],
      )),
    );
  }
}
