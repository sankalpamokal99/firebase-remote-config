import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_remote_config/remote_config_service.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  static FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:
          Color(int.parse(RemoteConfigService().getBackgroundColorValue))
              .withOpacity(0.5),
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const SizedBox(
              height: 50,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15.0),
              child: Text(
                RemoteConfigService().getTitleValue,
                style: const TextStyle(fontSize: 30, color: Colors.white),
              ),
            ),
            Expanded(
                child:
                    Image.network(RemoteConfigService().getNetworkImageValue)),
            ElevatedButton(
              onPressed: () {
                print("Button enabled");
                _sendAnalyticsEvent('Jio');
              },
              child: RemoteConfigService().getEnableButtonValue
                  ? const Text("I am clickable")
                  : const Text("I am not clickable"),
            ),
            const SizedBox(
              height: 50,
            )
          ],
        ),
      ),
    );
  }

  Future<void> _sendAnalyticsEvent(String compName) async {
    await firebaseAnalytics.setUserProperty(
        name: 'company_name', value: compName);
    print('company_name triggered $compName');
  }
}
