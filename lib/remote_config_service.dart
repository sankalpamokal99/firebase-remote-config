import 'dart:convert';

import 'package:firebase_remote_config/firebase_remote_config.dart';

const String _ENABLE_BUTTON = 'enable_button';
const String _BACKGROUND_COLOR = 'background_color';
const String _NETWORK_IMAGE = 'network_image';
const String _TITLE = 'title_text';
const String _JSON_Value = 'json_value';

class RemoteConfigService {
  static final RemoteConfigService _remoteConfigService =
      RemoteConfigService._internal();

  final FirebaseRemoteConfig _firebaseRemoteConfig =
      FirebaseRemoteConfig.instance;

  factory RemoteConfigService() {
    return _remoteConfigService;
  }

  RemoteConfigService._internal();

  static RemoteConfigService get() {
    return _remoteConfigService;
  }

  static final defaults = <String, dynamic>{
    _ENABLE_BUTTON: true,
    _BACKGROUND_COLOR: "0xFF2196F3",
    _NETWORK_IMAGE:
        "https://images.unsplash.com/photo-1612441804231-77a36b284856?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8bW91bnRhaW4lMjBsYW5kc2NhcGV8ZW58MHx8MHx8&w=1000&q=80",
    _TITLE: "Flutter Firebase",
  };

  bool get getEnableButtonValue =>
      _firebaseRemoteConfig.getBool(_ENABLE_BUTTON);
  String get getBackgroundColorValue =>
      _firebaseRemoteConfig.getString(_BACKGROUND_COLOR);
  String get getNetworkImageValue =>
      _firebaseRemoteConfig.getString(_NETWORK_IMAGE);
  String get getTitleValue => _firebaseRemoteConfig.getString(_TITLE);
  RemoteConfigValue get getJsonValue =>
      _firebaseRemoteConfig.getValue(_JSON_Value);

  Future initialize() async {
    try {
      await _firebaseRemoteConfig.setDefaults(defaults);
      await _fetchAndActivate();
    } on Exception catch (e) {
      print("Rmeote Config fetch throttled: $e");
    } catch (e) {
      print("Unable to fetch remote config. Default value will be used");
    }
  }

  Future _fetchAndActivate() async {
    await _firebaseRemoteConfig.setConfigSettings(RemoteConfigSettings(
      fetchTimeout: const Duration(minutes: 1),
      minimumFetchInterval: const Duration(
          minutes:
              3), // will remain cache for minimumFetchInterval even though value changed from dashboard
    ));
    await _firebaseRemoteConfig.fetchAndActivate();
    print("getBackgroundColorValue::: $getBackgroundColorValue");
    print("getEnableButtonValue::: $getEnableButtonValue");
    print("getNetworkImageValue::: $getNetworkImageValue");
    print("getTitleValue::: $getTitleValue");
    print("getJsonValue::: ${jsonDecode(getJsonValue.asString())}");
  }
}
